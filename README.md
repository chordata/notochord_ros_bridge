# notochord_ros_bridge

This package is used to convert OSC messages from notochord to ROS messages and joint state messages.




## Setup
Go into your workspace source folder (e.g. catkin_ws/src): <br>
`cd ~/catkin_ws/src` <br>
clone this repository: <br>
`git clone https://gitlab.com/chordata/notochord_ros_bridge` <br>
return in catkin_ws: <br>
`cd ..` <br>
check dependencies: <br>
`rosdep install --from-paths src --ignore-src -r -y` <br>
make: <br>
`catkin_make` <br>
install: <br>
`catkin_make install` <br>

## Nodes

### notochord_ros_bridge_node
***How to run:*** <br>
While `roscore` is running: <br>
`rosrun notochord_ros_bridge notochord_ros_bridge_node` <br>

***Topics:***
- `/chordata/rot/data` (published), check NotochordData message type in msg folder

***Params:***
- `~/port` (*default* 7000), OSC port
- `~/address` (*default* /chordata/rot), name of OSC topic
- `~/frame_id` (*default* world), ros frame id

### notochord_ros_joint
***How to run:*** <br>
While `roscore` is running: <br>
`rosrun notochord_ros_bridge notochord_ros_joint` <br>

***Topics:***
- `/chordata/rot/data` (subscribed), check NotochordData message type in msg folder
- `/joint_states` (published), message to control joints

***Params:***
- `~/mode` (*default* 0), mode used to compute joints angle, 0 -> osc data used directly


## Launch

Display URDF file: <br>
`roslaunch notochord_ros_bridge display.launch`
<br>

Connect to osc: <br>
`roslaunch notochord_ros_bridge osc_to_ros.launch`
<br>

Run all the package (osc -> ros -> joints -> rviz): <br>
`roslaunch notochord_ros_bridge notochord_ros_all.launch`

Run minimal configuration (osc -> ros -> joints): <br>
`roslaunch notochord_ros_bridge minimal.launch`


## Run on Raspberry Pi 3

Check the file **raspberry_guide.md**
<br>
<br>
<br>
This package was tested on:
- Intel N4200 with Ubuntu 18.04 & ROS Melodic (desktop full installation)
- Intel N4100 with Ubuntu 20.04 & ROS Noetic (desktop full installation)
- Raspberry Pi 3 with issues in locating packages and more than one time compilation (e.g. random error happen recompiling disappear)
<br>
<br>
<br>

__Copyright (c) 2021 Giovanni di Dio Bruno__
