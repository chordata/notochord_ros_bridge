/*
 * 
 * This is software was created by Giovanni di Dio Bruno - gbr1.github.io
 * © G. Bruno 2021
 * 
 */
/*
 * This node is used to publish joints from /notochord/rot/data message
 * 
 */
#include <urdf/model.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <string>
#include "notochord_ros_bridge/NotochordJoint.h"
#include "notochord_ros_bridge/NotochordJointTree.h"
#include "notochord_ros_bridge/NotochordData.h"
#include "notochord_ros_bridge/NotochordQuaternion.h"
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

ros::Publisher pub;
bool notInExec = true;
bool dataAvailable = false;
std::vector<notochord_ros_bridge::NotochordQuaternion> work_msg;

int mode=0;


void oscCallback(const notochord_ros_bridge::NotochordData::ConstPtr& msg){
    if (notInExec){
        try{
            work_msg.resize(msg->data.size());
            std::copy(msg->data.begin(), msg->data.end(), work_msg.begin());
        }
        catch(const std::exception& e){
            ROS_ERROR("%s",e.what());
        }
        dataAvailable=true;
    }
    else{
        ROS_WARN("Callback: discarded messages. Joints loop is too slow");
    }
}

int main(int argc, char** argv){
    ros::init(argc, argv, "notochord_armature_joint");
    ros::NodeHandle nh;

    // Get the param for evaluate if differantial computational is not required needed (0->use data directly, other->compute differencies)
    if (nh.getParam("/notochord_ros_joint/mode",mode)){
        ROS_INFO("Compute mode: %d", mode);
    }  
    else{
        mode=0;
        ROS_INFO("[Default] Compute mode: %d", mode);
    }

    // Load urdf
    urdf::Model model;
    if (!model.initParam("/robot_description")){
        ROS_ERROR("Failed to parse urdf file");
        return -1;
    }
    ROS_INFO("Successfully parsed urdf file\r\n\r\n");


    // Adds all joints to NotochordJointList
    std::map<std::string, urdf::JointSharedPtr> model_joints=model.joints_;
    
    // Create a tree
    NotochordJointTree joint_tree(mode);
    
    std::list<urdf::JointSharedPtr> joints;
    bool checkP=false;

    // Check for root and order in RPY joints
    for (const auto& [name,jointSP]: model_joints){
        urdf::JointSharedPtr jsp = jointSP;
        if (jsp->parent_link_name.compare(model.getRoot()->name)==0){
            NotochordJoint tmp_joint;
            tmp_joint.updateFromString(jsp->name);
            joint_tree.node=tmp_joint;
        }
        else{
            if ((jsp->name.back()=='R')&&(checkP)){
                urdf::JointSharedPtr tmp_jsp=joints.back();
                joints.pop_back();
                joints.push_back(jsp);
                joints.push_back(tmp_jsp);
            }
            else{
                joints.push_back(jsp);
            }
            checkP=false;
            if (jsp->name.back()=='P'){
                checkP=true;
            }
        }
    } 

    // Fill the tree nodes
    for (const auto& joint: joints){
        joint_tree.insert(joint);
    }

    // Print the tree
    ROS_INFO("generated OSC dependencies tree");
    joint_tree.printTree();
    ROS_INFO("end of tree\r\n\r\n");

    // Create the ROS joints list
    std::list<std::string> joint_list;
    joint_tree.list(joint_list);

    // Print the list
    ROS_INFO("movable joint loaded:");
    for (const auto & elem: joint_list){
        ROS_INFO("%s",elem.c_str());
    }
    ROS_INFO("done.\r\n\r\n");

    // Advertise the publisher
    pub = nh.advertise<sensor_msgs::JointState>("joint_states", 1);

    // Attach the subscriber
    ros::Subscriber sub = nh.subscribe("/chordata/rot/data", 1, oscCallback);
    
    // ROS node loop
    while(ros::ok()){
        if (dataAvailable){
            notInExec=false;
            //exec
            sensor_msgs::JointStatePtr joint_msg = boost::make_shared<sensor_msgs::JointState>();
            joint_msg->header.stamp=ros::Time::now();
            joint_msg->header.frame_id="base_link"; //need to be checked
            joint_msg->name.resize(joint_list.size());
            joint_msg->position.resize(joint_list.size());
                     
            // Load quaternions into joint_tree
            for(auto & element : work_msg){
                joint_tree.updateQuaternion(element);
            }

            joint_tree.computeAll();
        
            // Prepare joint_state message to be published
            std::list<double> data_joint_list;
            joint_tree.getTreeData(data_joint_list);
            std::list<std::string>::iterator index_joints=joint_list.begin();
            std::list<double>::iterator index_data_joints=data_joint_list.begin();
            for (int i=0; i<joint_list.size();i++){
                joint_msg->name[i]=index_joints->c_str();
                joint_msg->position[i]=*index_data_joints;
                index_joints++;
                index_data_joints++;
            }

            // Publish
            pub.publish(joint_msg);
            dataAvailable=false;
            notInExec=true;
        }

        // Update ROS
        ros::spinOnce();
    }

    // Everything ok!
    return 0;
}
