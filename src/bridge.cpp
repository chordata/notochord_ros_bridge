/*
 * 
 * This is software was created by Giovanni di Dio Bruno - gbr1.github.io
 * © G. Bruno 2021
 * 
 */

/* bridge.cpp is compiled as notochord_ros_bridge_node and allows to stream OSC data on a ROS network.
 * Usage is simple: rosrun notochord_ros_bridge notochord_ros_bridge_node
 *
 */


#include <iostream>
#include <cstring>

#include "notochord_ros_bridge/OscReceiverParser.h"
#include "ip/UdpSocket.h"

#include <ros/ros.h>
#include "notochord_ros_bridge/NotochordData.h"


int         port;           // OSC port
std::string address;        // OSC address root, e.g. /Chordata/rot
std::string frame_id;       // frame_id as TF reference, probably not so useful in this case


int main(int argc, char* argv[]){
    try{

        // Initialize ROS node and handle
        ros::init(argc, argv, "notochord_ros_bridge");
        ros::NodeHandle nh;

        // Get parameters
        if (nh.getParam("/notochord_ros_bridge/port",port)){
            ROS_INFO("OSC Port: %d", port);
        }  
        else{
            port=7000;
            ROS_INFO("[Default] OSC port: %d", port);
        }

        if (nh.getParam("/notochord_ros_bridge/address",address)){
            ROS_INFO("OSC address: %s", address.c_str());
        }  
        else{
            address="/Chordata/rot";
            ROS_INFO("[Default] OSC address: %s", address.c_str());
        }

        if (nh.getParam("/notochord_ros_bridge/frame_id",frame_id)){
            ROS_INFO("OSC frame_id: %s", frame_id.c_str());
        }  
        else{
            frame_id="world";
            ROS_INFO("[Default] OSC frame_id: %s", frame_id.c_str());
        }

        // Create the publisher
        ros::Publisher pub = nh.advertise<notochord_ros_bridge::NotochordData>("/chordata/rot/data", 1);
        
        // Create an OSC listener
        OscReceiverParser listener(address,pub,frame_id);

        // Attach OSC listener
        UdpListeningReceiveSocket s(IpEndpointName( IpEndpointName::ANY_ADDRESS, port), &listener );
        s.RunUntilSigInt();
    }
    //hey! there is something not working here!
    catch(const std::exception& e){
        ROS_ERROR("%s",e.what());
        return -1;
    }
    return 0;
}