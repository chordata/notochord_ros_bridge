**NOTE:** following guide is based on [http://wiki.ros.org/ROSberryPi/Installing%20ROS%20Melodic%20on%20the%20Raspberry%20Pi](http://wiki.ros.org/ROSberryPi/Installing%20ROS%20Melodic%20on%20the%20Raspberry%20Pi)

## Setup Raspberry Pi 3B
Follow instruction to setup your board: <br>
[https://wiki.chordata.cc/wiki/User_Manual/1._Chordata_parts/1.1._Microcomputer](https://wiki.chordata.cc/wiki/User_Manual/1._Chordata_parts/1.1._Microcomputer)


## Edit swap

`sudo dphys-swapfile swapoff`

`sudo nano /etc/dphys-swapfile` and edit `CONF_SWAPSIZE=1024`
exit and save (ctrl+x -> y -> enter)

`sudo dphys-swapfile setup`

`sudo dphys-swapfile swapon`

## Prepare ROS installation

`sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'`

`sudo -E  apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654`

`sudo apt update`

`sudo apt upgrade`

`sudo apt install -y python-rosdep python-rosinstall-generator python-wstool python-rosinstall build-essential cmake`

`sudo rosdep init`

`rosdep update`

## Install ROS

`mkdir -p ~/ros_catkin_ws`

`cd ~/ros_catkin_ws`

`rosinstall_generator ros_comm --rosdistro melodic --deps --wet-only --tar > melodic-ros_comm-wet.rosinstall`

`wstool init src melodic-ros_comm-wet.rosinstall`

`rosdep install -y --from-paths src --ignore-src --rosdistro melodic -r --os=debian:buster`

`sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/melodic -j2`

`source /opt/ros/melodic/setup.bash`

`echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc`

## Install notochord_ros_bridge dependencies

`rosinstall_generator ros_comm sensor_msgs geometry_msgs tf urdf roslint topic_tools tf2_geometry_msgs robot_state_publisher --rosdistro melodic --deps --wet-only --tar > melodic-custom_ros.rosinstall`

`wstool merge -t src melodic-custom_ros.rosinstall`

`wstool update -t src`

`rosdep install --from-paths src --ignore-src --rosdistro melodic -y -r --os=debian:buster`

`sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/melodic`

*NOTE: here you have time for a coffee, walking, etc.*

## Install notochord_ros_bridge

`cd ~/ros_catkin_ws/src`

`git clone https://gitlab.com/chordata/notochord_ros_bridge`

`cd ..`

`wstool merge -t src melodic-custom_ros.rosinstall`

`wstool update -t src`

`rosdep install --from-paths src --ignore-src --rosdistro melodic -y -r --os=debian:buster`

`sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/melodic`

Your Raspberry Pi 3B is now ready!

## Setup ROS for remote

On your Raspberry Pi 3B:

`sudo nano .bashrc` and add:

`export ROS_MASTER_URI=http://notochord.local:11311`

`export ROS_HOSTNAME=notochord.local`

`source .bashrc`



<br>

On your remote computer:

`sudo nano .bashrc` and add:

`export ROS_MASTER_URI=http://notochord.local:11311`

`export ROS_HOSTNAME=<your computer ip>`

`source .bashrc`












