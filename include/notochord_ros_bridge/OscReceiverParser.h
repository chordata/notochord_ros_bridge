#ifndef __OSCRECEIVERPARSER_H__
#define __OSCRECEIVERPARSER_H__

/*
 * 
 * This is software was created by Giovanni di Dio Bruno - gbr1.github.io
 * © G. Bruno 2021
 * 
 */


/* This class is used to create a listener for OSC and a ROS publisher. 
 * NotochordData.msg topic format is used and all data are in sync.
 * It is possibile to use any OSC packet length.
 * 
 * Constructor parameters:
 * - osc_topic -> the intial part of the OSC packet, e.g. /Chordata/rot
 * - pub -> a notochord_ros_bridge::NotochordData ROS publisher
 * - frame_id -> associated ROS frame
 * 
 * The ros::Time is picked when the first message of the OSC packet is read, e.g. when base is listened
 * 
 * Quaternion data in OSC should be in w x y z format.
 * 
 */


#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"

#include <ros/ros.h>
#include "notochord_ros_bridge/NotochordData.h"
#include "notochord_ros_bridge/NotochordQuaternion.h"

#include <list>
#include <string>

class OscReceiverParser : public osc::OscPacketListener{
    private:
        std::string address;
        std::string topic;
        float q[4];

        std::list<std::string> receivedList;
        std::list<std::string>::iterator it;
        
        notochord_ros_bridge::NotochordDataPtr data_ros;
        ros::Publisher  pub;    
        std::string frame_id;

        std::string osc_topic;
        int cut_osc_topic;

    protected:
        virtual void ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint ){
            (void) remoteEndpoint;
            try{
                // get the address pattern
                address=std::string(m.AddressPattern());
                
                // lookup if it is the right one, e.g. /Chordata/rot 
                if (address.find(osc_topic)>=0){

                    // get the stream and unpack
                    osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
                    args >> q[0] >> q[1] >> q[2] >> q[3] >> osc::EndMessage;
                    topic = address.substr(cut_osc_topic);
                    
                    // check if it is the first message of a packet, e.g. base
                    if (!receivedList.empty()){

                        // reset list iterator
                        it=receivedList.begin();

                        // are we back to the first sensor???
                        if (it->compare(topic.c_str())==0){

                            // publish ROS topic
                            pub.publish(data_ros);
                            ros::spinOnce();
                            
                            //clear lists
                            receivedList.clear(); 
                            data_ros->data.clear();
                        }
                    }
                    else{
                        // it is the first message so let's create the header of ROS topic (get ros::Time)
                        data_ros->header.frame_id=frame_id;
                        data_ros->header.stamp=ros::Time::now();
                    }

                    // add sensor topic to the list
                    receivedList.push_back(topic);

                    // append data to the ROS message
                    notochord_ros_bridge::NotochordQuaternion quaternion_ros;
                    quaternion_ros.frame_id=topic.c_str();
                    quaternion_ros.quaternion.w=q[0];
                    quaternion_ros.quaternion.x=q[1];
                    quaternion_ros.quaternion.y=q[2];
                    quaternion_ros.quaternion.z=q[3];
                    data_ros->data.push_back(quaternion_ros);
                    
                    // verbose
                    ROS_INFO("OSC raw %s: %f,%f,%f,%f",topic.c_str(), q[0], q[1], q[2], q[3]); 
                    
                }
            }catch( osc::Exception& e ){
                ROS_ERROR("error while parsing message: %s: %s",m.AddressPattern(),e.what());
            }
        }
    
    public:
        OscReceiverParser(std::string _osc_topic, ros::Publisher  _pub, std::string _frame_id) : osc::OscPacketListener(){
            osc_topic=_osc_topic+"/";
            cut_osc_topic=osc_topic.length();
            pub=_pub;
            frame_id=_frame_id;
            data_ros = boost::make_shared<notochord_ros_bridge::NotochordData>();
        }
};

#endif