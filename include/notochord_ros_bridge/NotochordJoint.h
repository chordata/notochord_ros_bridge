/*
 * 
 * This is software was created by Giovanni di Dio Bruno - gbr1.github.io
 * © G. Bruno 2021
 * 
 */
#ifndef __NOTOCHORDJOINT_H__
#define __NOTOCHORDJOINT_H__

#include <string>
#include <tf2/LinearMath/Quaternion.h>


class NotochordJoint{
    private:
        std::string urdfName;
        std::string urdfNameR;
        std::string urdfNameP;
        std::string urdfNameY;
        std::string oscName;
        bool R,P,Y;
    
    public:
        double roll, pitch, yaw; 
        tf2::Quaternion osc_quaternion;
        tf2::Quaternion relative_quaternion;

        NotochordJoint(){
            urdfName="";
            urdfNameR="";
            urdfNameP="";
            urdfNameY="";
            oscName="";
            R=false;
            P=false;
            Y=false;
            roll=0;
            pitch=0;
            yaw=0;
        }

        NotochordJoint(std::string name){
            updateFromString(name);
        }

        void setUrdfName(const std::string name){urdfName=name;}
        void setUrdfNameRoll(const std::string name){urdfNameR=name;}
        void setUrdfNamePitch(const std::string name){urdfNameP=name;}
        void setUrdfNameYaw(const std::string name){urdfNameY=name;}
        void setOscName(const std::string name){oscName=name;}

        std::string getUrdfName(){return urdfName;}
        std::string getUrdfNameRoll(){return urdfNameR;}
        std::string getUrdfNamePitch(){return urdfNameP;}
        std::string getUrdfNameYaw(){return urdfNameY;}
        std::string getOscName(){return oscName;}

        void setRoll(const double r){roll=r;}
        void setPitch(const double p){pitch=p;}
        void setYaw(const double y){yaw=y;}

        double getRoll(){return roll;}
        double getPitch(){return pitch;}
        double getYaw(){return yaw;}


        void setR(const bool r){R=r;}
        void setP(const bool p){P=p;}
        void setY(const bool y){Y=y;}

        void setRPY(const std::string s,const std::string joint_name_RPY){
            if(s.compare("R")==0){
                setR(true);
                setUrdfNameRoll(joint_name_RPY);
            }
            if(s.compare("P")==0){
                setP(true);
                setUrdfNamePitch(joint_name_RPY);
            }
            if(s.compare("Y")==0){
                setY(true);
                setUrdfNameYaw(joint_name_RPY);
            }
        }

        bool hasR(){return R;}
        bool hasP(){return P;}
        bool hasY(){return Y;}

        std::string hasRPYasString(){
            std::string srpy="";
            if(hasR()){
                srpy+="R";
            }
            else{
                srpy+="-";
            }
            if(hasP()){
                srpy+="P";
            }
            else{
                srpy+="-";
            }
            if(hasY()){
                srpy+="Y";
            }
            else{
                srpy+="-";
            }
            return srpy;
        }

        void updateFromString(const std::string name){
            std::string part1=name.substr(name.find_first_of('_')+1);        //joint
            std::string part2=part1.substr(part1.find_first_of('_')+1);      //xx
            if (part2.find('_')!=std::string::npos){
                setOscName(part2.substr(0,part2.find_first_of('_')));
                setRPY(part2.substr(part2.find_first_of('_')+1),name);
                setUrdfName(name.substr(0,name.find_last_of('_')));
            }
            else{
                setOscName(part2);
                setUrdfName(name);
            }
        }


        NotochordJoint& operator=(NotochordJoint& j){
            setUrdfName(j.getUrdfName());
            setUrdfNameRoll(j.getUrdfNameRoll());
            setUrdfNamePitch(j.getUrdfNamePitch());
            setUrdfNameYaw(j.getUrdfNameYaw());
            setOscName(j.oscName);
            setR(j.R);
            setP(j.P);
            setY(j.Y);
            
            return *this;
        }
};


std::string unpackToOsc(const std::string s){
    std::string part1=s.substr(s.find_first_of('_')+1);        //joint
    std::string part2=part1.substr(part1.find_first_of('_')+1);      //xx
    if (part2.find('_')!=std::string::npos){
        return part2.substr(0,part2.find_first_of('_'));
    }
    else{
        return part2;
    }
}





#endif