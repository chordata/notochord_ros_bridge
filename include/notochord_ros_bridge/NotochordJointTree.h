/*
 * 
 * This is software was created by Giovanni di Dio Bruno - gbr1.github.io
 * © G. Bruno 2021
 * 
 */
#ifndef __NOTOCHORDJOINTTREE_H__
#define __NOTOCHORDJOINTTREE_H__

#include <list>
#include "NotochordJoint.h"
#include <urdf/model.h>
#include <string>
#include "notochord_ros_bridge/NotochordQuaternion.h"
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>



class NotochordJointTree{ 
    private:
        int mode;  
    public:
        NotochordJoint node;
        std::list<NotochordJointTree> children;

        NotochordJointTree(){mode=0;}
        NotochordJointTree(const int _m){mode=_m;}
        
        bool isFinal(){
            if (children.size()>0){
                return true;
            }
            return false;
        }
    
        void printTree(const int level=0){
            std::string s="";
            s+=node.hasRPYasString()+"  ";
            for (int i=0; i<level; i++){
                s+=" => ";
            }
            s+=node.getOscName();
            ROS_INFO("%s",s.c_str());
            for (auto & e:children){
                e.printTree(level+1);
            }
        }

        void insert(urdf::JointSharedPtr joint){
            std::string full_name=joint->name;
            std::string parent=unpackToOsc(joint->parent_link_name);
            NotochordJoint noto_joint;
            noto_joint.updateFromString(joint->name);
            if (node.getOscName().empty()){
                node=noto_joint;
                std::cout<<parent<<std::endl;
            }
            else{
                if (noto_joint.getOscName().compare(node.getOscName())==0){
                    //update R P Y
                    node.updateFromString(full_name);
                }
                else{
                    //check parent
                    if (parent.compare(node.getOscName())==0){
                        NotochordJointTree tmp_tree(mode);
                        tmp_tree.node=noto_joint;
                        children.push_back(tmp_tree);
                    }
                    else{
                        for(auto& child:children){
                            child.insert(joint);
                        }
                    }
                }
            }
        }


        // This method generates a list containing all movable joints
        void list(std::list<std::string> & joint_list){
            if (node.hasR()){
                joint_list.push_back(node.getUrdfNameRoll());
            }
            if (node.hasP()){
                joint_list.push_back(node.getUrdfNamePitch());
            }
            if (node.hasY()){
                joint_list.push_back(node.getUrdfNameYaw());
            }
            for(auto& child: children){
                child.list(joint_list);
            }
            
        }

        // Retrieve data from the tree
        void getTreeData(std::list<double> & joint_data){
            if (node.hasR()){
                joint_data.push_back(node.getRoll());
            }
            if (node.hasP()){
                joint_data.push_back(node.getPitch());
            }
            if (node.hasY()){
                joint_data.push_back(node.getYaw());
            }
            for(auto& child: children){
                child.getTreeData(joint_data);
            }
        }

        // Update values
        void updateQuaternion(notochord_ros_bridge::NotochordQuaternion q){
            if (q.frame_id.compare(node.getOscName())==0){
                tf2::fromMsg(q.quaternion, node.osc_quaternion);
            }
            else{
                for(auto& child: children){
                    child.updateQuaternion(q);
                }
            }
        }

        // Elaborate the tree
        void compute(){
            for(auto& child: children){   
                if(mode!=0){
                    child.node.relative_quaternion = (child.node.osc_quaternion)*node.osc_quaternion.inverse();
                    tf2::Matrix3x3 rot_mat(child.node.relative_quaternion);
                    rot_mat.getRPY(child.node.roll,child.node.pitch,child.node.yaw);
                }
                else{
                    tf2::Matrix3x3 rot_mat(child.node.osc_quaternion);
                    rot_mat.getRPY(child.node.roll,child.node.pitch,child.node.yaw);
                }
                child.compute();
            }  
        }

        // Elaborate the root
        void computeRoot(){
            tf2::Matrix3x3 rot_mat(node.osc_quaternion);
            node.relative_quaternion=node.osc_quaternion;
            rot_mat.getRPY(node.roll,node.pitch,node.yaw);
        }

        // Compute everything
        void computeAll(){
            computeRoot();
            compute();
        }
    
};

#endif